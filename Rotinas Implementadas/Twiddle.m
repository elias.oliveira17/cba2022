%% PARÂMETROS MÓDULO FOTOVOLTAICO
clc; clear all; close all; format longe;
%Características de Catálogo
Vmp = 26.3; Imp = 7.61; Voc = 32.9; Isc = 8.21; Pmp = Vmp*Imp;
Ns = 54; alpha = 3.18e-3; beta = -1.23e-1; T = 25; G = 1000;
param = [Pmp Vmp Imp Voc Isc Ns alpha beta T G];

% Resistências Candidatas
Rscand = 1e-3:1e-3:10e-3;
Rpcand = 1:2:20;
RsRpsel = [];
%% PARAMETROS DO TWIDDLE
%Diferencial de alteração de 'dR', caso este falhe
ksi = 30/100;
%Limiar de finalização
b = 6; Delta = 1*10^(-b);

conTot = 0;
for i=1:length(Rscand)
   for j=1:length(Rpcand)
       % Resistências Rs e Rp
       R = [Rscand(i) Rpcand(j)];
       %Diferenciais de alteração dos parâmetros R
       dRi = [1e-5 1e-3];
       dR = dRi;

       %Obtem o primeiro erro. Como é o unico existente, é o melhor.
       melhorErro = simulaPlanta(R,param,0);
       erroInicial = melhorErro;

       % LOOP DE OTIMIZACAO
       clc
       cont = 0;
       while abs(sum(dR)) > Delta
           %Contador de iterações do laço e global
           cont = cont + 1;
           conTot = conTot + 1;

           for k = 1:length(R)
               %Procura pelos melhores parâmetros afrente do original

               %Incrementa o parâmetro R
               if(k==1)
                   R(k) = limita(R(k) + dR(k),Rscand(end),1e-9);
               else
                   R(k) = limita(R(k) + dR(k),Rpcand(end),1e-9);
               end
               %Calcula o erro associado ao novo conjunto de parâmetros
               Erro = simulaPlanta(R,param,0);

               %Se o erro calculado no momento atual é melhor que o melhor erro
               %calculado até o momento (melhorErro), então, substitui-se o erro
               if Erro < melhorErro
                   %Armazena o erro
                   melhorErro = Erro;
                   %Aumenta o diferencial dR
                   dR(k) = dR(k)*(1+ksi);     
                   
                   %Se o erro melhorou (diminuiu), então significa que estamos
                   %procurando o parâmetro no sentido correto. Logo, aumentar a
                   %variação deste parâmetro se mostra coerente.
               else
                   %PROCURA PELOS MELHORES PARÂMETROS NA DIREÇÃO CONTRÁRIA

                   %Inverte o sentido do parâmetro R(i)
                   if(k==1)
                       R(k) = limita(R(k) - 2*dR(k),Rscand(end),1e-9);
                   else
                       R(k) = limita(R(k) - 2*dR(k),Rpcand(end),1e-9);
                   end

                   %Calcula o erro associado ao novo parâmetro
                   Erro = simulaPlanta(R,param,0);

                   %Se o erro calculado é o melhor que o melhor erro até o momento
                   if Erro < melhorErro
                       %Armazena o erro
                       melhorErro = Erro;
                       %Aumenta o diferencial dR
                       dR(k) = dR(k)*(1+ksi);
                   else
                       %Nenhum dos sentidos funcionou
                       %Retorna o parâmetro R(i) para seu valor original
                       if(k==1)
                           R(k) = limita(R(k) + dR(k),Rscand(end),1e-9);
                       else
                           R(k) = limita(R(k) + dR(k),Rpcand(end),1e-9);
                       end
                       %Diminui o diferencial dR(i)
                       dR(k) = dR(k)*(1-ksi);   
                   end
               end
           end
           fprintf('i: %d/%d, j: %d/%d - It %i(%i): M Erro = %.*f, soma(dR) = %.*f, Rs = %.5f, Rp = %.5f \n', i, length(Rscand), j, length(Rpcand), cont, conTot, b, melhorErro, b,sum(dR),R(1), R(2))
       end
       % Armazena histórico de resistências encontradas
       fprintf('parâmetros: Rs = %.5f, Rp = %.5f \n', R(1), R(2))
       RsRpsel = [RsRpsel; R];

   end
end
conTot
%% PLOT simulaPlanta
% Obtenção de curvas e pontos de interesse
caracSel = []; erroH = [];
for i=1:length(RsRpsel)
    R = RsRpsel(i,:);
    [erro, PVI] = simulaPlanta(R,param, 1); 
    k = find(PVI(1,:)==max(PVI(1,:)),2,'first'); k = k(end);
    iVoc = find(min(abs(PVI(2,:)-Voc))==abs(PVI(2,:)-Voc)); iVoc = iVoc(end);
    %                     i   Pmp      Vmp      Imp      Voc      Isc
    caracSel = [caracSel; i PVI(1,k) PVI(2,k) PVI(3,k) PVI(2,iVoc) PVI(3,1)];
    erroH = [erroH; erro];
end
disp('    i       Pmp          Vmp      Imp      Voc          Isc')
disp(caracSel)

% Localiza o índice do menor erro médio
MelhorMedErro = min(erroH)
iMelhorMedErro = find(erroH==MelhorMedErro);
iMelhorMedErro = iMelhorMedErro(end)

% Atualiza o valor das resistências associadas ao menor erro médio
R = RsRpsel(iMelhorMedErro,:);

% Exporta para arquivo csv (cria histórico externo)
data = clock;
data = sprintf('       %d/%02.0f/%02.0f %02.0fh%02.0fm%02.0fs',data);
res = num2str([caracSel(iMelhorMedErro,2:end), ksi, Delta, conTot, R, dRi]);
% alternativo = sprintf('       %d',alternativo)
res = [res data]
resTxt = fopen('resultados.csv','a');
fprintf(resTxt,'%s \n',res);
fclose(resTxt);

% Obtém curvas para o menor erro médio
[erro, PVI] = simulaPlanta(R,param, 1); 
iMPP = find(PVI(1,:)==caracSel(iMelhorMedErro,2));
PVI = round(PVI,3);

% Plot das curvas IV e PV com dois eixos habilitados
close
figure(1)
configs;
hold on, grid on, grid minor, yyaxis right
pv_plot = plot(PVI(2,:),PVI(1,:),'linewidth',2);
datatip(pv_plot,'DataIndex',iMPP,'location','northeast');
datatip(pv_plot,caracSel(iMelhorMedErro,5),0,'location','northwest');
ylabel('Pot\^encia [W]'), xlabel('Tens\~ao [V]'),ylim([0 1.05*max(PVI(1,:))])
yyaxis left
iv_plot = plot(PVI(2,:),PVI(3,:),'linewidth',2);
datatip(iv_plot,'DataIndex',iMPP,'location','southwest');
datatip(iv_plot,0,caracSel(iMelhorMedErro,6),'location','northeast');
ylabel('Corrente [A]')
configs;
saveas(gca,'PVI','pdf');
