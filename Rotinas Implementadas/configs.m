sf = 18;
sfdt = 16;

% configs datatip
alldatacursors = findall(gcf,'type','hggroup');
set(alldatacursors,'FontSize', sfdt);

% configs labels e title
set(0, 'defaultTextInterpreter', 'latex');
set(groot,'DefaultAxesTickLabelInterpreter','latex');
set(gca,'DefaultTextFontSize',sf)

% configs layout 
set(gcf,'PaperSize',3*[4 3]);
set(gcf,'PaperPosition',[0 0 get(gcf,'PaperSize')]);
set(gca, 'FontSize', sf);
fig1.Renderer = 'Painters';
