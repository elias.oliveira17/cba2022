clc, clear all, close all
% RESISTÊNCIAS MODELAGEM
Rs =  0.0042747577; Rp = 11.922685;
R = [Rs Rp];
% CARACTERÍSTICAS DE CATÁLOGO
Vmp = 26.3; Imp = 7.61; Voc = 32.9; Isc = 8.21; Pmp = Vmp*Imp;
Ns = 54; alpha = 3.18e-3; beta = -1.23e-1; T = 25; G = 1000;
param = [Pmp Vmp Imp Voc Isc Ns alpha beta T G];
% CENÁRIOS DE IRRADIAÇÃO E TEMPERATURA
Gc = [1000 800 600 1000 1000];
Tc = [25   25  25   50   75];
% LEGENDAS
lg1 = cell(1,1*3);
lg2 = cell(1,1*2);
lg2 = {'Fabricante','Villalva (2009)'};
lg3 = cell(1,3);
lg4 = lg2;
yM = 0;
% LAÇO PARA SIMULAÇÃO/COMPARAÇÃO DE CENÁRIOS
for i=1:length(Gc)
    % SIMULA A PLANTA PARA O LAÇO ATUAL DE IRRADIÇÃO E TEMPERATURA
    param([9,10]) = [Tc(i), Gc(i)];
    [~,PVI] = simulaPlanta(R,param, 1);
    Vpa = round(PVI(2,:),3)';
    Ipa = round(PVI(3,:),3)';
    % VARIAÇÕES DE IRRADIAÇÃO
    if(i<=3)
        figure(1)
        plot(Vpa,Ipa,'linewidth',2)
        lg1{i} = sprintf('$%d$ W/m$^2$',Gc(i));
        if(yM<max(Ipa))
            yM = max(Ipa);
            ylim([0 1.1*yM])
        end
        hold on,  grid on, grid minor   , configs
        ylabel('Corrente [A]'), xlabel('Tens\~ao [V]'), configs
        if(i==3)
            legend(lg1,'location','southwest'),set(legend,'interpreter','latex')
        end
        if(i==1)
            figure(3)
            plot(Vpa,Ipa,'linewidth',2)
            lg3{i} = sprintf('$%d$ $^\\circ$C',Tc(i));
            hold on,  grid on, grid minor   , configs
        end
        
        dp = cell(1,2);
        dp = {strcat('dataPoints/data/Dataset',num2str(i-1),'.csv'); strcat('dataPoints/villalvaCalc/Dataset',num2str(i-1),'.csv')};
        
        for k=1:length(dp)
            VId = load(dp{k});
            in = [];
            for x=1:length(VId)
                y = find(min(abs(VId(x,1)-Vpa))==abs(VId(x,1)-Vpa)); y = y(end);
                in = [in; y x];
            end
            % CÁLCULO ERRO ABSOLUTO
            erroI = abs(Ipa(in(:,1))-VId(in(:,2),2));
            
            figure(2)
            subplot(1,3,i)
            plot(Vpa(in(:,1)),erroI,'-+','linewidth',1,'markersize',6)
            hold on,  grid on, grid minor, configs
            ylabel('Erro Absoluto: $|I_{calc}-I_{amost}|$ [A]'),xlabel('Tens\~ao [V]'),title(sprintf('$%d$ W/m$^2$',Gc(i)))
            if(k==length(dp))
                legend(lg2,'location','northwest'),set(legend,'interpreter','latex')
            end
            % CÁLCULO OCORRÊNCIA DE ERROS MENORES OU IGUAIS A 'crp'%
            crp = 10;
            cr = sum(100*erroI./VId(in(:,2),2)<=crp)/length(erroI)*100;
            
            %             crp = 0.1;
            %             cr = sum(erroI<=crp)/length(erroI)*100;
            
            %             fprintf('%s G Tc: %d %d, Erro medio: %.5f, Desvio Padrão: %.5f, Erro<=%d: %.5f \n',dp{k},Pc(i), Tc(i), mean(100*erroI./VId(in(:,2),2)),std(100*erroI./VId(in(:,2),2)),crp,cr)
            fprintf('%s G Tc: %d %d, Erro medio: %.5f, Desvio Padrão: %.5f, Erro<=%d: %.5f \n',dp{k},Gc(i), Tc(i), mean(erroI),std(erroI),crp,cr)
            
        end
        % VARIAÇÕES DE TEMPERATURA
    else
        figure(3)
        plot(Vpa,Ipa,'linewidth',2)
        lg3{i-2} = sprintf('$%d$ $^\\circ$C',Tc(i));
        if(yM<max(Ipa))
            yM = max(Ipa);
            ylim([0 1.1*yM])
        end
        hold on,  grid on, grid minor   , configs
        ylabel('Corrente [A]'), xlabel('Tens\~ao [V]'), configs
        if(i==length(Gc))
            legend(lg3,'location','southwest'),set(legend,'interpreter','latex')
        end
        
        dp = cell(1,2);
        dp = {strcat('dataPoints/data/Dataset',num2str(i-1),'.csv'); strcat('dataPoints/villalvaCalc/Dataset',num2str(i-1),'.csv')};
        
        for k=1:length(dp)
            VId = load(dp{k});
            
            in = [];
            for x=1:length(VId)
                y = find(min(abs(VId(x,1)-Vpa))==abs(VId(x,1)-Vpa)); y = y(end);
                in = [in; y x];
            end
            % CÁLCULO ERRO ABSOLUTO
            erroI = abs(Ipa(in(:,1))-VId(in(:,2),2));
            
            figure(4)
            subplot(1,2,i-3)
            plot(Vpa(in(:,1)),erroI,'-+','linewidth',1,'markersize',6)
            hold on,  grid on, grid minor, configs
            ylabel('Erro Absoluto: $|I_{calc}-I_{amost}|$ [A]'),xlabel('Tens\~ao [V]'),title(sprintf('$%d$ $^\\circ$C',Tc(i)))
            if(k==length(dp))
                legend(lg4,'location','northwest'),set(legend,'interpreter','latex')
            end
            % CÁLCULO OCORRÊNCIA DE ERROS MENORES OU IGUAIS A 'crp'%
            cr = sum(100*erroI./VId(in(:,2),2)<=crp)/length(erroI)*100;
            
            %             cr = sum(erroI<=crp)/length(erroI)*100;
            
            %             fprintf('%s G Tc: %d %d, Erro medio: %.5f, Desvio Padrão: %.5f, Erro<=%d: %.5f \n',dp{k},Pc(i), Tc(i), mean(100*erroI./VId(in(:,2),2)),std(100*erroI./VId(in(:,2),2)),crp,cr)
            fprintf('%s G Tc: %d %d, Erro medio: %.5f, Desvio Padrão: %.5f, Erro<=%d: %.5f \n',dp{k},Gc(i), Tc(i), mean(erroI),std(erroI),crp,cr)
            
        end
    end
end

for i=1:4
    figure(i)
    nm = strcat('fig',num2str(i)); saveas(gca,nm,'pdf');
end

%%
close all
clc
param = [Pmp Vmp Imp Voc Isc Ns alpha beta T G];
[~,PVI] = simulaPlanta(R,param, 1);
Vpa = round(PVI(2,:),3)';
Ipa = round(PVI(3,:),3)';

regFI = [0, 0;
    0.99*Vmp, 0;
    0.99*Vmp, 1.05*max(Ipa);
    0, 1.05*max(Ipa)];

regFV = [1.01*Vmp, 0;
    1.05*max(Vpa), 0;
    1.05*max(Vpa), 1.05*max(Ipa);
    1.01*Vmp, 1.05*max(Ipa)];

c1 = '#000000';
c2 = 1.1*[0.7 0.7 0.7];
figure(5)
fill(regFI(:,1), regFI(:,2),c2);
hold on
configs;
fill(regFV(:,1), regFV(:,2),c2)
plot(Vpa, Ipa, 'k', 'linewidth', 2)
plot(Vmp, Imp, 'ok', 'markersize', 10, 'linewidth', 2)
plot(0, Isc, 'ok', 'markersize', 8, 'linewidth', 2)
plot(Voc, 0, 'ok', 'markersize', 8, 'linewidth', 2)
% plot([Vmp*ones(size([0:Imp/100:Imp]))], [0:Imp/100:Imp], '--', 'linewidth', 1, 'color', c1)
% plot([0:Vmp/100:Vmp], [Imp*ones(size([0:Vmp/100:Vmp]))], '--', 'linewidth', 1, 'color', c1)
text(mean(regFI(:,1)),mean(regFI(:,2)), 'Fonte de Corrente')
text(0.9*mean(regFV(:,1)),1.7*mean(regFV(:,2)), 'Fonte de Tens\~ao')
% text(Vmp-2.5, Imp-0.3, 'MPP')
xlabel('Tens\~ao [V]'), ylabel('Corrente [A]')
xticks([Vmp Voc]), yticks([Imp Isc])
xticklabels({'$V_{mp}$','$V_{oc}$'}), 
yticklabels({'$I_{mp}$','$I_{sc}$'})
xlim([0 1.05*max(Vpa)]), ylim([0 1.05*max(Ipa)])
saveas(gca,'IVgen','pdf')
