function [delErro, PVI] = simulaPlanta(R,parametros,dados)
% RESISTÊNCIAS MODELAGEM
Rs = R(1); Rp = R(2);
% AJUSTE DA CARACTERÍSTICA IV E DADOS DE CATALOGO
Pmp = parametros(1); Vmp = parametros(2); Imp = parametros(3);
Ms = 1; Mp = 1; Ns = parametros(6);
Voc = parametros(4)/Ns; Isc = parametros(5);
alpha = parametros(7);
beta = parametros(8);
% VARIÁVEIS DE ENTRADA
T = parametros(9); G = parametros(10);
amostras = 1e5; passo = Voc*Ns/amostras; Vpa = 0:passo:Voc*Ns;
% CONSTANTES
n = 1.3;
k1 = 1.3806503e-23;
q = 1.60217646e-19;
EG = 1.121;
Tr = 273.15 + 25;
Gr = 1000;
% CALCULOS DE TENSÕES E CORRENTES DE INTERESSE
T = 273.15 + T;
Vt = n*k1*T/q;
Vtr = n*k1*Tr/q;
V = Vpa/Ms/Ns;
Isc_ = Isc+alpha*(T-Tr);
Voc_ = (Voc*Ns + beta*(T-Tr))/Ns;
Iph = Isc_*G/Gr;
Ir = (Isc_-Voc_/Rp)/(exp(Voc_/Vt)-1);
% Irr = (Isc-Voc/Rp)/(exp(Voc/Vtr)-1);
% Ir = Irr*(T/Tr)^3*exp(q*EG/n/k1*(1/Tr-1/T));

I = zeros(size(V));
for j=1:10;
    I = I-(Iph-I-Ir.*(exp((V+I.*Rs)./Vt)-1)-(V+I.*Rs)./Rp)./(-1-Ir.*exp((V+I.*Rs)./Vt).*Rs./Vt-Rs./Rp);
end
Ipa = I*Mp;
Ppa=Vpa.*Ipa;

% CÁLCULO PVI NA MÁXIMA POTÊNCIA, VOC, ISC E ERROS ASSOCIADOS
Pmp_calc = max(Ppa);
k = find(Pmp_calc==Ppa); k = k(end);
Vmp_calc = Vpa(k);
Imp_calc = Ipa(k);
erroP = abs(Pmp-Pmp_calc);
erroV = abs(Vmp-Vmp_calc);
erroI = abs(Imp-Imp_calc);
Isc_calc = Ipa(1);
erroIsc = abs(Isc_-Isc_calc);
iVoc = find(min(abs(Vpa-Voc_*Ns))==abs(Vpa-Voc_*Ns)); iVoc = iVoc(end);
% iVoc = find(min(abs(Vpa-Voc*Ns))==abs(Vpa-Voc*Ns)); iVoc = iVoc(end);
% erroVoc = abs(Voc*Ns-Vpa(iVoc));
erroVoc = abs(Voc_*Ns-Vpa(iVoc));
E = mean([erroP erroV erroI erroIsc erroVoc]);

% Caso dados=1, retornamos as curvas PVI
if(isequal(dados,1))
    PVI = [Ppa; Vpa; Ipa];
end

% Lógica de retorno dos erros envolvidos
if(erroP<1/100 & erroV<1/100 & erroI<1/100)
    if(erroIsc<1/100)
        delErro = E;
    else
        delErro = 50*E;
    end
else
    delErro = 100*E;
end

end
